#include "MyString.h"
#include "func_utils.h"

const int NB_OCCURENCES = 26;

MyString::MyString()
{
    str = NULL;
    length_str = 0;
    occurences = NULL;
    carac_spe = 0;
}
MyString::MyString(char* tab)
{
    char *p = NULL, *cpy = tab;
    int *p_o = NULL;

    length_str = str_length(tab);
    str = p = new char[length_str];
    p_o = occurences = new int[NB_OCCURENCES];
    carac_spe = 0;

    for(int i = 0; i < NB_OCCURENCES; i++, p_o++)
        *p_o = 0;
    p_o = occurences;
    for(int i = 0; i < length_str; i++, p++, cpy++)
    {
        int value;

        *p = *cpy;
        if(IsSpecialChar(*cpy))
            carac_spe++;
        value = getLetterIndex(*cpy);
        if(value != -1)
        {
            p_o += value;
            *p_o = *p_o + 1;
            p_o = occurences;
        }
    }
}
MyString::MyString(char letter, int n)
{
    char *p = NULL;
    int value = 0, *m = NULL;
    length_str = n;
    str = p = new char[n];
    m = occurences = new int[NB_OCCURENCES];
    if(IsSpecialChar(letter))
        carac_spe = n;
    for(int i = 0; i < length_str; i++, p++)
        *p = letter;

    value = getLetterIndex(letter);
    if(value != -1)
    {
        m += value;
        *m = n;
    }
}
MyString::MyString(const MyString &another_string)
{
    char *p = NULL, *m = NULL;
    int *occu = another_string.occurences, *pt = NULL;

    length_str = another_string.length_str;
    carac_spe = another_string.carac_spe;
    m = another_string.str;

    str = p = new char[length_str];
    pt = occurences = new int[NB_OCCURENCES];

    for(int i = 0; i < length_str; i++, p++, m++)
        *p = *m;

    for(int i = 0; i < 26; i++, pt++, occu++)
        *pt = *occu;
}
MyString::~MyString()
{
    delete str;
    delete occurences;
}
MyString& MyString::operator=(const MyString &Second_String)
{
    if(this != &Second_String)
    {
        char* m = Second_String.str, *p = NULL;
        int* occu = Second_String.occurences, *pt = NULL;
        delete str;
        delete occurences;
        length_str = Second_String.length_str;
        carac_spe = Second_String.carac_spe;

        str = p = new char[length_str];
        pt = occurences = new int[NB_OCCURENCES];

        for(int i = 0; i < length_str; i++, p++, m++)
            *p = *m;

        for(int i = 0; i < 26; i++, pt++, occu++)
            *pt = *occu;
    }
    return *this;
}
MyString& MyString::operator=(char* other_str)
{
    delete this;
    char *p = NULL, *cpy = other_str;
    int *p_o = NULL;

    length_str = str_length(other_str);
    str = p = new char[length_str];
    p_o = occurences = new int[NB_OCCURENCES];
    carac_spe = 0;

    for(int i = 0; i < NB_OCCURENCES; i++, p_o++)
        *p_o = 0;
    p_o -= NB_OCCURENCES;
    for(int i = 0; i < length_str; i++, p++, cpy++)
    {
        int value;

        *p = *cpy;
        if(IsSpecialChar(*cpy))
            carac_spe++;
        value = getLetterIndex(*cpy);
        if(value != -1)
        {
            p_o += value;
            *p_o = *p_o + 1;
            p_o = occurences;
        }
    }
    return *this;
}
MyString MyString::operator+(MyString& Second_String)
{
    MyString res(*this);
    res.concatenate(Second_String);
    return res;
}
MyString& MyString::operator+=(MyString& Second_String)
{
    concatenate(Second_String);
    return *this;
}
MyString MyString::operator-(char letter)
{
    MyString res(*this);
    res.deleteChar(letter);
    return res;
}
MyString& MyString::operator-=(char letter)
{
    deleteChar(letter);
    return *this;
}
ostream &operator <<(ostream &MyStream, const MyString &mystr)
{
    char *cpy = mystr.str;
    for(int i = 0; i < mystr.length_str; i++, cpy++)
        MyStream << *cpy;
    return MyStream;
}
int operator!(const MyString &mystr)
{
    return mystr.length_str;
}
MyString MyString::operator*(char letter)
{
    MyString res(*this);
    res.doubleChar(letter);
    return res;
}
MyString& MyString::operator*=(char letter)
{
    doubleChar(letter);
    return *this;
}
void MyString::upperCase()
{
    char *p = str;
    for(int i = 0; i < length_str; i++, p++)
        if(*p >= 'a' && *p <= 'z')
            *p = *p - 32;
}
void MyString::lowerCase()
{
    char *p = str;
    for(int i = 0; i < length_str; i++, p++)
        if(*p >= 'A' && *p <= 'Z')
            *p = *p + 32;
}
void MyString::show()
{
    char *p = str;
    for(int i = 0; i < length_str; i++, p++)
        cout << *p;
    cout << endl;
}
void MyString::deleteChar(char letter)
{
    char *p = NULL, *cpy = NULL;
    int nb = 0, i;
    cpy = str;
    int new_length = 0, value = 0;
    int *occu = occurences;

    for(i = 0; i < length_str; i++, cpy++)
        if(*cpy == letter)
            nb++;
    cpy = str;
    new_length = length_str - nb;

    str = p = new char[new_length];
    for(int i = 0; i <length_str; i++, cpy++)
    {
        if(*cpy != letter)
        {
            *p = *cpy;
            p++;
        }
    }
    if(IsSpecialChar(letter))
        carac_spe -= nb;
    value = getLetterIndex(letter);
    if(value != -1)
    {
        occu += value;
        *occu = 0;
    }
    cpy -= length_str;
    length_str = new_length;
    delete cpy;
}
void MyString::showOccurences()
{
    int *p = occurences;
    for(int i = 0; i < 26; i++, p++)
        cout << (char)(i + 65) << " : " << *p << endl;
    cout << "Caract�res sp�ciaux : " << carac_spe << endl;
}
void MyString::doubleChar(char letter)
{
    char *p = NULL, *cpy = NULL;
    int nb = 0, i;
    cpy = str;
    int new_length = 0;

    for(i = 0; i < length_str; i++, cpy++)
        if(*cpy == letter)
            nb++;
    cpy -= i;
    new_length = length_str + nb;
    delete str;
    str = p = new char[new_length];
    for(i = 0; i < length_str; i++, p++, cpy++)
    {
        *p = *cpy;
        if(*cpy == letter)
        {
            p++;
            *p = letter;
        }
    }
    if(IsSpecialChar(letter))
        carac_spe += nb;
    value = getLetterIndex(letter);
    if(value != -1)
    {
        occu += value;
        *occu = *occu * 2;
    }
    length_str = new_length;
}
int MyString::getLength()
{
    return length_str;
}
int MyString::getCaracSpec()
{
    return carac_spe;
}
void MyString::concatenate(const MyString &other_string)
{
    int new_length = length_str + other_string.length_str;
    char *p, *res, *cpy = str, *other_cpy = other_string.str;
    int *pt = occurences, *other_pt = other_string.occurences;

    p = res = new char[new_length];
    for(int i = 0; i < length_str; i++, p++, cpy++)
        *p = *cpy;
    for(int i = 0; i < other_string.length_str; i++, p++, other_cpy++)
        *p = *other_cpy;
    for(int i = 0; i < NB_OCCURENCES; i++, pt++, other_pt++)
        *pt += *other_pt;
    delete str;
    str = res;
    carac_spe += other_string.carac_spe;
    length_str = new_length;
}
