#include "func_utils.h"

int str_length(char *str)
{
    int length = 0;
    while(*str != '\0')
    {
        length++;
        str++;
    }
    return length;
}
int getLetterIndex(char letter)
{
    int res = -1;
    if(letter >= 'A' && letter <= 'Z')
        res = ((int) letter) - 65;
    else if(letter >= 'a' && letter <= 'z')
        res = ((int)letter) - 97;
    return res;
}
bool IsSpecialChar(char letter)
{
    if(!(letter == ' ' || ((letter >= 'A' && letter <= 'Z') || (letter >= 'a' && letter <= 'z') || (letter >= '0' && letter <= '9'))))
        return true;
    return false;
}
