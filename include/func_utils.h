#ifndef FUNC_UTILS_H
#define FUNC_UTILS_H

int str_length(char* str);
int getLetterIndex(char letter);
bool IsSpecialChar(char letter);

#endif // FUNC_UTILS_H
