#ifndef MYSTRING_H
#define MYSTRING_H
#include <iostream>

using namespace std;
class MyString
{
    friend ostream& operator<<(ostream&, const MyString&);
    friend int operator!(const MyString&);

    private:
    char *str;
    int length_str, *occurences, carac_spe;

    public:
    MyString(char* tab);
    MyString();
    MyString(const MyString &another_string);
    MyString(char letter, int n);
    ~MyString();
    int getLength();
    int getCaracSpec();
    void show();
    void deleteChar(char letter);
    void doubleChar(char letter);
    void concatenate(const MyString &other_string);
    void updateCaracSpe();
    void showOccurences();
    void upperCase();
    void lowerCase();

    MyString& operator=(const MyString&);
    MyString& operator=(char*);
    MyString operator+(MyString&);
    MyString& operator+=(MyString&);
    MyString& operator-=(char);
    MyString operator-(char);
    MyString operator*(char);
    MyString& operator*=(char);
};

#endif // MYSTRING_H
